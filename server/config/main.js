/**
 * Store global settings (CONFIG)
 */

const BLOCKCHAIN_URL = 'wss://api.i9networks.net.br';
//const BLOCKCHAIN_URL = '';

const SYNC_DATABASE = false;

module.exports = {
	BLOCKCHAIN_URL,
	SYNC_DATABASE
};
