/* eslint-disable */
require('dotenv').config()
const chalk = require('chalk');
const path = require('path');

const express = require('express');
const webpack = require('webpack');
const bodyParser = require('body-parser');

const config = require('../webpack.config');

const mysql = require('mysql');

const {ChainTypes} = require('peerplaysjs-lib');
const Blockchain = require("./api");
const db = require("./database/constants");
const config_server = require("./config/main");
const utils = require("./utility/utils")

const app = express();
const compiler = webpack(config);
const router = express.Router();
const port = process.env.PORT || 5000;
var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./swagger.json');

// ===== CONFIG/MIDDLEWARE =====

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// app.use('/api/v1', router);

// ===== ROUTES =====
const blocks = require("./routes/blocks");
const accounts = require("./routes/accounts");
const witnesses = require("./routes/witnesses");
const operations = require("./routes/operations");
const transactions = require("./routes/transactions");
const utility = require("./routes/utility");
const contracts = require("./routes/contracts");


app.use("/api", [blocks, accounts, witnesses, operations, transactions, utility, contracts]);

// ===== SYNC FUNCTIONS =====

/* Takes in a MYSQL connection and attempts to sync the database non-block tables with the blockchain 
	This function should be used sparingly, as it performs a full sync.
*/

async function syncDatabase(connection) {

	let accountNames = await Blockchain.getAccountNamesRecursively('', 1000);
	let nameAry = [];
	for (const name of accountNames) {
		if (!db.RESTRICTED.includes(name[0])) {
			nameAry.push(name[0]);
		}
	}

	// Accounts
	const startIndex = 0;
	const endIndex = 100;
	const fullAccountList = [];
	const r1 = await Blockchain.getFullAccountsRecursively(nameAry, startIndex, endIndex, fullAccountList);
	r1.forEach(async (data, index) => {
		if (data && data[1].account) {

		data = data[1].account;

			const account_name = data.name;
			const membership_expiration_date = data.membership_expiration_date;
			const referrer = data.referrer;
			const owner_key = data.owner.key_auths.length > 0 ? data.owner.key_auths[0][0] : '';
			const active_key = data.active.key_auths.length > 0 ? data.active.key_auths[0][0] : '';
			const memo_key = data.options.memo_key;
			const account_id = data.id;

			// console.log(account_id);
			const member_since = await Blockchain.getRegDate(account_id, '1.11.0');
			// console.log(member_since);

			sql = `SELECT * FROM accounts WHERE account_name = '${account_name}'`
			connection.query(sql, function (err, result) {
			  if (err) {
				  throw err;
			  }
			//   console.log("Result: " + JSON.stringify(result));

			  if (result.length < 1) { // Insert data
			sql = `INSERT INTO accounts (account_name, membership_expiration, referrer, owner_key, active_key, memo_key, member_since, account_id)
			VALUES ('${account_name}', '${membership_expiration_date}', '${referrer}', '${owner_key}', '${active_key}', '${memo_key}', '${member_since}', '${account_id}')`;

				connection.query(sql, function(err, result) {

					if (err) {
						throw err;
					}

				})
			  }
			});
		}
	});

	let witnessNames = await Blockchain.getWitnessesRecursively('', 1000);
	let witnessAry = [];
	for (const witness of witnessNames) {
		witnessAry.push(witness[1]); // 0 = name, 1 = id
	}

	// Witnesses
	const r2 = await Blockchain.getWitnessObjsById(witnessAry); 

	r2.forEach(async (data, index) => {
		// build witness object
		const account_id = witnessNames[index][1];
		const account_name = witnessNames[index][0];
		const witness = data.witness_account;
		const witness_since = await Blockchain.getWitnessDate(witness, '1.11.0');
		const total_votes = data.total_votes;
		const url = data.url;
		// const is_active = 
		const total_missed = data.total_missed;

		sql = `SELECT * FROM witnesses WHERE account_name = '${account_name}'`
		connection.query(sql, function (err, result) {
		if (err) {
			throw err;
		}
		// console.log("Result: " + JSON.stringify(result));

		if (result.length < 1) { // Insert witness data
			sql = `INSERT INTO witnesses (account_id, account_name, witness, witness_since, total_votes, total_missed, url, is_active)
			VALUES ('${account_id}', '${account_name}', '${witness}', '${witness_since}', '${total_votes}', '${total_missed}', '${url}', '${1}');`

			connection.query(sql, function (err, result) {
				if (err) {
					throw err;
				}
		});
	
	}


	})
});


  // Fees
  let r3 = await Blockchain.getGlobalProperties();

  let feeAry = [];
 	 r3.parameters.current_fees.parameters.map((feeObj) => {
	 if (feeObj.length > 0) {
		feeObj[1] = JSON.stringify(feeObj[1]);
		feeAry.push((feeObj));
	 }
  })
  const feeAryLength = feeAry.length;
  let feeValues = '';
  feeAry.forEach((fee, index) => {
	  const operationName = utils.getKeyByValue(ChainTypes.operations, fee[0])
	  if(operationName) {
		  if(index === feeAryLength - 1) {
			feeValues = feeValues + `('${fee[0]}', '${utils.getKeyByValue(ChainTypes.operations, fee[0])}', '${fee[1]}')`
		  } else {
			feeValues = feeValues + `('${fee[0]}', '${utils.getKeyByValue(ChainTypes.operations, fee[0])}', '${fee[1]}'),`
		  }
	  }
  })
  feeValues = feeValues.slice(0, -1);
  
//   sql = `SELECT * FROM operations`;
  
//   connection.query(sql, function (err, result) {
// 	if (err) {
// 		throw err;
// 	}

	// if (result.length < 1) { // Insert data
  	sql = `INSERT IGNORE INTO operations (id, friendly_name, current_fees) VALUES${feeValues}`

	connection.query(sql, function (err, result) {
		if (err) {
			throw err;
		}
	});
	// }
//   });



// Committee
let r4 = await Blockchain.getCommittee();

r4.forEach(async (data, index) => {
	sql = `INSERT IGNORE INTO committee (committee_id, committee_member_account, vote_id, total_votes, url) VALUES ('${data.id}', '${data.committee_member_account}', '${data.vote_id}', '${data.total_votes}', '${data.url}') ON DUPLICATE KEY UPDATE    
	committee_id='${data.id}', committee_member_account='${data.committee_member_account}', vote_id='${data.vote_id}', total_votes='${data.total_votes}', url='${data.url}'`

	connection.query(sql, function (err, result) {
		if (err) {
			throw err;
		}
});
})

console.log('Exeplorer Server> Done non-block sync.')
return;
}


// ===== SERVER STARTUP =====
if (process.env.NODE_ENV !== 'production') {
	require('./config/dev.js')(app);
} else {
	app.use(express.static(path.resolve(__dirname, '..', 'build')));
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(compiler.outputPath, 'index.html'));
	});
}

app.listen(port, err => {
	if (err) {
		return console.error(err);
	} 
  console.log(chalk.green(`App is listening on: ${port}`)); 

  const connection = mysql.createConnection({
	host     : db.HOST,
	user     : db.USER,
	password : db.PASSWORD,
	database : db.DATABASE,
	connectTimeout: 10000
  });

connection.connect(function(err) {
	if (err) {
		console.error('error connecting to DB: ' + err.stack);
		return;
	}
  
	console.log(chalk.blueBright(`Exeplorer Server> Connected to DB: ${connection.threadId}`));
});

Blockchain.connect(config_server.BLOCKCHAIN_URL).then(async () => {

	if (process.env.SYNC == 'true') {
		streaming = false;
		console.log('Sync mode is ON');
		await syncDatabase(connection);

		let sql = `SELECT block_number FROM blocks ORDER BY BLOCK_NUMBER DESC LIMIT 1`;
		connection.query(sql, function (err, result) {
			if (result && result[0]) {
				result = result[0].block_number;
	
			} else {
				result = 0;
			}
	
			if (err) {
				throw err;
			}
			console.log(chalk.blueBright(`Exeplorer Server> Starting from block #: ${result+1}`))
			Blockchain.populateBlocks(connection, result, '', streaming);
			
		});

	} else if(process.env.SYNC == 'false' && process.env.STREAM == 'true') {
			streaming = true;
			await syncDatabase(connection);

			Blockchain.getObject('2.1.0', (error, dynamicGlobal) => {
				startingBlock = dynamicGlobal.head_block_number - 100;
				console.log(chalk.blueBright(`Exeplorer Server> Starting from block #: ${startingBlock}`))

				Blockchain.populateBlocks(connection, startingBlock, '', streaming);
			});
	}
});
	

});

module.exports = app;
